<!DOCTYPE html>
<html class="css-bckg">
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="../aesthetics/css.css">
	<title>Witcher 3 Bestiary</title>
</head>
<body class="css-body">
	<div class="div-form-2"><img src="../img/medallion.jpg" class="img-medallion"><img src="../img/logo.png" class="img-logo"><img src="../img/wandciri.jpg" class="img-wandciri">
	</div>
	<div class="div-form">
		<?php

		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "witcher";

// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 

		$sql = "SELECT * FROM bestiary";
		$result = $conn->query($sql);

		$i = 0;
		if ($result->num_rows > 0) {
 // output data of each row
			while($row = $result->fetch_assoc()) {
				$Name[$i] = $row["Name"];
				$Descr[$i] = $row["Descr"];
				$Lore[$i] = $row["Lore"];
				$Id[$i] = $row["Id"];
				$i = $i + 1;
			}
		} else {
		}
		$conn->close();


		$numberSongs = $i;

		if($numberSongs > 0){
			for ($i = 0; $i < $numberSongs; $i++) { 
				echo "<button class='btn btn-default'><a href='../players.php?id=$Name[$i]'>$Name[$i]</button>";
		}
	}else{
		echo "<h3>Error</h3>";
	}?>
</div>
</body>
</html>